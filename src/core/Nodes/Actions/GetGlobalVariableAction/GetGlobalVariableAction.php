<?php

namespace Drupal\if_then_else\core\Nodes\Actions\GetGlobalVariableAction;

use Drupal\if_then_else\core\Nodes\Actions\Action;
use Drupal\if_then_else\Event\GraphValidationEvent;
use Drupal\if_then_else\Event\NodeSubscriptionEvent;
use Drupal\Component\Utility\Html;
use Drupal\if_then_else\Event\NodeValidationEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Get variable action class.
 */
class GetGlobalVariableAction extends Action {
  use StringTranslationTrait;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct($ifthenelse_global_variables) {
    $this->global_variables = $ifthenelse_global_variables;
  }

  /**
   * {@inheritdoc}
   */
  public static function getName() {
    return 'get_global_variable_action';
  }

  /**
   * {@inheritdoc}
   */
  public function registerNode(NodeSubscriptionEvent $event) {
    $event->nodes[static::getName()] = [
      'label' => $this->t('Get Global Variable Action'),
      'description' => $this->t('Get global variable action'),
      'type' => 'action',
      'class' => 'Drupal\\if_then_else\\core\\Nodes\\Actions\\GetGlobalVariableAction\\GetGlobalVariableAction',
      'library' => 'if_then_else/GetGlobalVariableAction',
      'control_class_name' => 'GetGlobalVariableActionControl',
      'outputs' => [
        'value' => [
          'label' => $this->t('Output'),
          'description' => $this->t('Value of the field set in the text field.'),
          'socket' => 'string',
        ]        
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateGraph(GraphValidationEvent $event) {
    $nodes = $event->data->nodes;
    foreach ($nodes as $node) {
      if ($node->data->type == 'value' && $node->data->name == 'text_value') {
        // To check empty input.
        foreach ($node->outputs->text->connections as $connection) {
          if ($connection->input == 'name' &&  (!property_exists($node->data, 'value') || empty($node->data->value))) {
            $event->errors[] = $this->t('Enter name in "@node_name".', ['@node_name' => $node->name]);
          }
          if ($connection->input == 'value' &&  (!property_exists($node->data, 'value') || empty($node->data->value))) {
            $event->errors[] = $this->t('Enter value in "@node_name".', ['@node_name' => $node->name]);
          }
        }
      }
    }
  }

  /**
   * {@inheritDoc}.
   */
  public function process() {
    $variable_name = Html::escape($this->data->valueText);
    $output = $this->global_variables[$variable_name];
    $this->outputs['value'] = $output;
  }

}
