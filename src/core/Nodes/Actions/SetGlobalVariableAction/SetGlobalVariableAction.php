<?php

namespace Drupal\if_then_else\core\Nodes\Actions\SetGlobalVariableAction;

use Drupal\if_then_else\core\Nodes\Actions\Action;
use Drupal\if_then_else\Event\NodeSubscriptionEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\if_then_else\Event\GraphValidationEvent;
use Drupal\if_then_else\Event\NodeValidationEvent;
use Drupal\Component\Utility\Html;

/**
 * Class defined to set global variable action node.
 */
class SetGlobalVariableAction extends Action {
  use StringTranslationTrait;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(&$ifthenelse_global_variables) {
    $this->global_variables = &$ifthenelse_global_variables;
  }

  /**
   * {@inheritdoc}
   */
  public static function getName() {
    return 'set_global_variable_action';
  }

  /**
   * {@inheritdoc}
   */
  public function registerNode(NodeSubscriptionEvent $event) {
    $event->nodes[static::getName()] = [
      'label' => $this->t('Set Global Variable Action'),
      'description' => $this->t('Set global variable action'),
      'type' => 'action',
      'class' => 'Drupal\\if_then_else\\core\\Nodes\\Actions\\SetGlobalVariableAction\\SetGlobalVariableAction',
      'library' => 'if_then_else/SetGlobalVariableAction',
      'control_class_name' => 'SetGlobalVariableActionControl',
      'inputs' => [
        'value' => [
          'label' => $this->t('Value'),
          'description' => $this->t('Input Value.'),
          'sockets' => ['string', 'bool', 'number', 'array', 'object.entity'],
          'required' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateGraph(GraphValidationEvent $event) {
    $nodes = $event->data->nodes;
    foreach ($nodes as $node) {
      if ($node->data->type == 'value' && $node->data->name == 'text_value') {
        // To check empty input.
        foreach ($node->outputs->text->connections as $connection) {
          if ($connection->input == 'name' &&  (!property_exists($node->data, 'value') || empty($node->data->value))) {
            $event->errors[] = $this->t('Enter name in "@node_name".', ['@node_name' => $node->name]);
          }
          if ($connection->input == 'value' &&  (!property_exists($node->data, 'value') || empty($node->data->value))) {
            $event->errors[] = $this->t('Enter value in "@node_name".', ['@node_name' => $node->name]);
          }
        }
      }
    }
  }

  /**
   * Validation function.
   */
  public function validateNode(NodeValidationEvent $event) {
    $data = $event->node->data;
  }

  /**
   * {@inheritDoc}.
   */
  public function process() {
    $variable_name = Html::escape($this->data->value);
    $this->global_variables[$variable_name] = $this->inputs['value'];
  }

}
